package palabras;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Libreria {

    /**
     * Carga en un array la lista de palabras de un archivo palabras.txt, si no existe, lo crea vacío.
     * @param fichero Fichero del que se lee la lista de las palabras.
     * @param listaPalabras Lista en la que se guardan las palabras leídas del fichero.
     * @return Lista generada con las palabras del archivo palabras.txt.
     * @throws IOException Se lanza este error cuando no se ha podido crear el archivo palabras.txt
     */

    public static ArrayList<Palabra> cargarPalabras(File fichero, ArrayList<Palabra> listaPalabras) throws IOException {

        if (fichero.exists()) {

            Scanner leerPalabras = new Scanner(fichero);

            while (leerPalabras.hasNext()) {
                listaPalabras.add(new Palabra(leerPalabras.next()));
            }
               /* for (String s : listaPalabras) {
                    System.out.println(s);
                }*/

        } else {
            fichero.createNewFile();
        }

        return listaPalabras;

    }

    /**
     * Añade una palabra al fichero y la añade a la lista de Palabras.
     * @param fichero Fichero al que se añade la palabra.
     * @param palabra Palabra a añadir.
     * @param palabras Lista de Palabras.
     *
     * @throws FileNotFoundException Excepción lanzada cuando no existe el fichero.
     */

    public static void anadirPalabra(File fichero, String palabra, ArrayList<Palabra> palabras) throws FileNotFoundException {
        PrintWriter escribirFichero = new PrintWriter(new FileOutputStream(fichero, true));
        palabra = palabra.toUpperCase();
        escribirFichero.println(palabra);
        escribirFichero.close();
        palabras.add(new Palabra(palabra));
    }


}
