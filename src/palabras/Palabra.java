package palabras;

import java.util.ArrayList;

public class Palabra implements Comparable<Palabra> {

    String dificultad;
    String nombre;
    ArrayList<Character> letras = new ArrayList<>();

    /**
     * Crea un objeto Palabra con un nombre.
     * @param nombre Palabra.
     */
    public Palabra(String nombre){
        this.nombre = nombre;
        if (nombre.length() < 4){
            this.dificultad = "facil";
        } else if (nombre.length() < 10){
            this.dificultad = "medio";
        } else {
            this.dificultad = "dificil";
        }

        for (int i = 0; i < (nombre.length()); i++){
            this.letras.add(nombre.charAt(i));
        }

    }

    public ArrayList<Character> getLetras(){

        if (letras.isEmpty()) {
            for (int i = 0; i < (nombre.length()); i++) {
                this.letras.add(nombre.charAt(i));
            }
        }

        return letras;
    }

    public String getDificultad() {
        return dificultad;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public int compareTo(Palabra p2){
        return this.nombre.compareTo(p2.nombre);
    }

}
